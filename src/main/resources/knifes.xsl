<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
      <h2>Knifes collection</h2>
      <table border = "1">
        <tr bgcolor = '#9acd32'>
          <th style="text-align:left">type</th>
          <th style="text-align:left">handle</th>
          <th style="text-align:left">origin</th>
          </tr>
          <xsl:for-each select="Knifes/Knife">
            <tr>
              <td><xsl:value-of select="type"/></td>
              <td><xsl:value-of select="handle"/></td>
              <td><xsl:value-of select="origin"/></td>
            </tr>
          </xsl:for-each>
      </table>
      </body>
    </html>
  </xsl:template>
  </xsl:stylesheet>