package parser;

import java.io.File;
import java.util.List;
import model.Knife;
import parser.DOM.DOMParser;
import parser.SAX.MySAXParser;
import parser.StAX.StAXParser;

public class Parser {

  public static void main(String[] args) {
    List<Knife> knives;
    File file = new File("src\\main\\resources\\knifes.xml");
    //DOM
    DOMParser domParser = new DOMParser(file);
    knives = domParser.getKnifeList();
    System.out.println("DOM Parser:");
    knives.stream().map(e -> e.getInfo()).forEach(System.out::println);
    //SAX
    MySAXParser saxParser = new MySAXParser(file);
    knives = saxParser.getKnifeList();
    System.out.println("SAX Parser:");
    knives.stream().map(e -> e.getInfo()).forEach(System.out::println);
    //StAX
    StAXParser stAXParser = new StAXParser(file);
    knives = stAXParser.getKnifeList();
    System.out.println("StAX Parser:");
    knives.stream().map(e -> e.getInfo()).forEach(System.out::println);
  }

}
