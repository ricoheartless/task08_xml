package parser.SAX;

import java.util.ArrayList;
import java.util.List;
import model.Blade;
import model.Handle;
import model.Knife;
import model.Visual;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

  private List<Knife> knifesList = null;
  private Knife knife = null;
  private Visual visual = null;
  private Blade blade = null;
  private Handle handle = null;


  public List<Knife> getKnifeList() {
    return knifesList;
  }

  boolean bType = false;
  boolean bHandy = false;
  boolean bOrigin = false;
  boolean bVisual = false;
  boolean bBladeCh = false; //Blade length and width section
  boolean bLength = false;
  boolean bWidth = false;
  boolean bMaterials = false; //Big material section
  boolean bBladeMaterial = false;
  boolean bHandle = false;
  boolean bHandleMaterial = false;
  boolean bHandleDetails = false;
  boolean bBloodstream = false;
  boolean bValue = false;

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {

    if (qName.equalsIgnoreCase("Knife")) {
      //create a new Employee and put it in Map
      String id = attributes.getValue("id");
      //initialize Employee object and set id attribute
      knife = new Knife();
      knife.setId(Integer.parseInt(id));
      //initialize list
      if (knifesList == null) {
        knifesList = new ArrayList<>();
      }
    } else if (qName.equalsIgnoreCase("type")) {
      //set boolean values for fields, will be used in setting Employee variables
      bType = true;
    } else if (qName.equalsIgnoreCase("handy")) {
      bHandy = true;
    } else if (qName.equalsIgnoreCase("origin")) {
      bOrigin = true;
    } else if (qName.equalsIgnoreCase("visual")) {
      bVisual = true;
    } else if (qName.equalsIgnoreCase("bladeChar")) {
      bBladeCh = true;
    } else if (qName.equalsIgnoreCase("length")) {
      bLength = true;
    } else if (qName.equalsIgnoreCase("width")) {
      bWidth = true;
    } else if (qName.equalsIgnoreCase("materials")) {
      bMaterials = true;
    } else if (qName.equalsIgnoreCase("blade")) {
      bBladeMaterial = true;
    } else if (qName.equalsIgnoreCase("handle")) {
      bHandle = true;
    } else if (qName.equalsIgnoreCase("material")) {
      bHandleMaterial = true;
    } else if (qName.equalsIgnoreCase("details")) {
      bHandleDetails = true;
    } else if (qName.equalsIgnoreCase("bloodstream")) {
      bBloodstream = true;
    } else if (qName.equalsIgnoreCase("value")) {
      bValue = true;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("Knife")) {
      //add Employee object to list
      knifesList.add(knife);
    }
  }

  @Override
  public void characters(char ch[], int start, int length) throws SAXException {

    if (bType) {
      //age element, set Employee age
      knife.setType(new String(ch, start, length));
      bType = false;
    } else if (bHandy) {
      knife.setHandy(new String(ch, start, length));
      bHandy = false;
    } else if (bOrigin) {
      knife.setOrigin(new String(ch, start, length));
      bOrigin = false;
    }
    else if (bBladeCh) {
      blade = new Blade();
      bBladeCh = false;
    }
    else if (bLength) {
      blade.setLength(Integer.parseInt(new String(ch,start,length)));
      bLength = false;
    }
    else if (bWidth) {
      blade.setWidth(Integer.parseInt(new String(ch,start,length)));
      bWidth = false;
    }else if (bBladeMaterial) {
      blade.setMaterial(new String(ch,start,length));
      bBladeMaterial = false;
    }else if (bHandleMaterial) {
      handle = new Handle();
      handle.setMaterial(new String(ch,start,length));
      bHandleMaterial = false;
    }else if (bHandleDetails) {
      handle.setDetail(new String(ch,start,length));
      bHandleDetails = false;
    }else if (bBloodstream) {
      blade.setBloodstream(Boolean.parseBoolean(new String(ch,start,length)));
      visual = new Visual(blade,handle);
      knife.setVisualChar(visual);
      bBloodstream = false;
    }else if (bValue) {
      String res = new String(ch,start,length);
      if(res.equalsIgnoreCase("collectible")){
        knife.setSerial(true);
      }
      else {
        knife.setSerial(false);
      }
      bValue = false;
    }

  }
}
