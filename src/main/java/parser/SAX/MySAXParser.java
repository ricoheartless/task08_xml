package parser.SAX;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import model.Knife;
import org.xml.sax.SAXException;
import parser.SAX.SaxHandler;

public class MySAXParser {
  List<Knife> knifeList;

  public List<Knife> getKnifeList() {
    return knifeList;
  }

  public MySAXParser(File file){
    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    try {
      javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser();
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(file, saxHandler);
      knifeList = saxHandler.getKnifeList();
    }
    catch (ParserConfigurationException | SAXException | IOException e){
      e.getStackTrace();
    }
  }

}
