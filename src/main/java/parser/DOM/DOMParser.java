package parser.DOM;

import java.io.File;
import java.util.List;
import model.Knife;

public class DOMParser {
  List<Knife> knifeList;

  public DOMParser(File file){
    DOMHandler domHandler = new DOMHandler(file);
    knifeList = domHandler.getKnifeList();
  }

  public List<Knife> getKnifeList() {
    return knifeList;
  }
}
