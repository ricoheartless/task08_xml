package parser.DOM;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Blade;
import model.Handle;
import model.Knife;
import model.Visual;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMHandler {

  List<Knife> knifeList;
  Knife knife;
  Visual visual;
  Handle handle;
  Blade blade;

  DOMHandler(File file) {
    knifeList = new ArrayList<>();
    try {
      File inputFile = file;
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(inputFile);
      doc.getDocumentElement().normalize();
      NodeList nodeList = doc.getElementsByTagName("Knife");
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if(node.getNodeType() == Node.ELEMENT_NODE){
          knife = new Knife();
          blade = new Blade();
          handle = new Handle();
          visual = new Visual();
          Element element = (Element)node;
          knife.setId(Integer.parseInt(element.getAttribute("id")));
          knife.setType(element.getElementsByTagName("type").item(0).getTextContent());
          knife.setHandy(element.getElementsByTagName("handy").item(0).getTextContent());
          knife.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
          blade.setLength(Integer.parseInt(
              element.getElementsByTagName("length").item(0).getTextContent())
          );
          blade.setWidth(Integer.parseInt(
          element.getElementsByTagName("width").item(0).getTextContent()));
          blade.setMaterial(
            element.getElementsByTagName("blade").item(0).getTextContent()
          );
          handle.setMaterial(
              element.getElementsByTagName("material").item(0).getTextContent()
          );
          handle.setDetail(
              element.getElementsByTagName("details").item(0).getTextContent()
          );
          blade.setBloodstream(Boolean.parseBoolean(
              element.getElementsByTagName("bloodstream").item(0).getTextContent()
              ));
          visual.setBlade(blade);
          visual.setHandle(handle);
          knife.setVisualChar(visual);
          knife.setSerial(
              isSerial(element.getElementsByTagName("value").item(0).getTextContent())
          );
          knifeList.add(knife);
        }
      }
    } catch (Exception e) {
      e.getStackTrace();
    }
  }

  public List<Knife> getKnifeList() {
    return knifeList;
  }

  private boolean isSerial(String s){
    if(s.equalsIgnoreCase("collectible")){
      return false;
  }
  return true;
  }
}
