package parser.StAX;

import java.io.File;
import java.util.List;
import model.Knife;

public class StAXParser {
  List<Knife> knifeList;
  public StAXParser(File file){
    StAXHandler stAXHandler = new StAXHandler(file);
    knifeList = stAXHandler.getKnifesList();
  }

  public List<Knife> getKnifeList() {
    return knifeList;
  }
}
