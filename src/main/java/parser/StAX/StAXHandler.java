package parser.StAX;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.Blade;
import model.Handle;
import model.Knife;
import model.Visual;

public class StAXHandler {
  Knife knife;
  Visual visual;
  Blade blade;
  Handle handle;
  List<Knife> knifesList;

  public List<Knife> getKnifesList() {
    return knifesList;
  }

  StAXHandler(File file){
    boolean bType = false;
    boolean bHandy = false;
    boolean bOrigin = false;
    boolean bVisual = false;
    boolean bBladeCh = false; //Blade length and width section
    boolean bLength = false;
    boolean bWidth = false;
    boolean bMaterials = false; //Big material section
    boolean bBladeMaterial = false;
    boolean bHandle = false;
    boolean bHandleMaterial = false;
    boolean bHandleDetails = false;
    boolean bBloodstream = false;
    boolean bValue = false;

    knife = null;
    visual = null;
    blade = null;
    handle = null;
    knifesList = new ArrayList<>();
    try {
      XMLInputFactory factory = XMLInputFactory.newInstance();
      XMLEventReader eventReader =
          factory.createXMLEventReader(new FileReader(file));
      while(eventReader.hasNext()) {
        XMLEvent event = eventReader.nextEvent();

        switch (event.getEventType()){
          case XMLStreamConstants.START_ELEMENT:
            StartElement startElement = event.asStartElement();
            String qName = startElement.getName().getLocalPart();
            if(qName.equalsIgnoreCase("Knife")) {
              Iterator<Attribute> attributes = startElement.getAttributes();
              String id = attributes.next().getValue();
              knife = new Knife();
              knife.setId(Integer.parseInt(id));
            }
         else if (qName.equalsIgnoreCase("type")) {
          bType = true;
        } else if (qName.equalsIgnoreCase("handy")) {
          bHandy = true;
        } else if (qName.equalsIgnoreCase("origin")) {
          bOrigin = true;
        } else if (qName.equalsIgnoreCase("visual")) {
          bVisual = true;
        } else if (qName.equalsIgnoreCase("bladeChar")) {
          bBladeCh = true;
        } else if (qName.equalsIgnoreCase("length")) {
          bLength = true;
        } else if (qName.equalsIgnoreCase("width")) {
          bWidth = true;
        } else if (qName.equalsIgnoreCase("materials")) {
          bMaterials = true;
        } else if (qName.equalsIgnoreCase("blade")) {
          bBladeMaterial = true;
        } else if (qName.equalsIgnoreCase("handle")) {
          bHandle = true;
        } else if (qName.equalsIgnoreCase("material")) {
          bHandleMaterial = true;
        } else if (qName.equalsIgnoreCase("details")) {
          bHandleDetails = true;
        } else if (qName.equalsIgnoreCase("bloodstream")) {
          bBloodstream = true;
        } else if (qName.equalsIgnoreCase("value")) {
          bValue = true;
        }
        break;
         case XMLStreamConstants.CHARACTERS:
            Characters characters = event.asCharacters();
           if (bType) {
             knife.setType(characters.getData());
             bType = false;
           } else if (bHandy) {
             knife.setHandy(characters.getData());
             bHandy = false;
           } else if (bOrigin) {
             knife.setOrigin(characters.getData());
             bOrigin = false;
           }
           else if (bBladeCh) {
             blade = new Blade();
             bBladeCh = false;
           }
           else if (bLength) {
             blade.setLength(Integer.parseInt(characters.getData()));
             bLength = false;
           }
           else if (bWidth) {
             blade.setWidth(Integer.parseInt(characters.getData()));
             bWidth = false;
           }else if (bBladeMaterial) {
             blade.setMaterial(characters.getData());
             bBladeMaterial = false;
           }else if (bHandleMaterial) {
             handle = new Handle();
             handle.setMaterial(characters.getData());
             bHandleMaterial = false;
           }else if (bHandleDetails) {
             handle.setDetail(characters.getData());
             bHandleDetails = false;
           }else if (bBloodstream) {
             blade.setBloodstream(Boolean.parseBoolean(characters.getData()));
             visual = new Visual(blade,handle);
             knife.setVisualChar(visual);
             bBloodstream = false;
           }else if (bValue) {
             String res = characters.getData();
             if(res.equalsIgnoreCase("collectible")){
               knife.setSerial(true);
             }
             else {
               knife.setSerial(false);
             }
             bValue = false;
           }
           break;
          case XMLStreamConstants.END_ELEMENT:
            EndElement endElement = event.asEndElement();
            if(endElement.getName().getLocalPart().equalsIgnoreCase("Knife")){
              knifesList.add(knife);
            }
            break;
        }
      }
    }
    catch (FileNotFoundException e) {
    e.printStackTrace();
  } catch (XMLStreamException e) {
    e.printStackTrace();
  }
  }

}
