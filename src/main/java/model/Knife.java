package model;

public class Knife {

  private int id;
  private String type;
  private String handy;
  private String origin;
  private Visual visualChar;
  private boolean isSerial;

  public Knife(int id, String type, String handy, String origin,
      Visual visual,
      boolean isSerial) {
    this.id = id;
    this.type = type;
    this.handy = handy;
    this.origin = origin;
    this.visualChar = visual;
    this.isSerial = isSerial;
  }

  @Override
  public String toString() {
    return "Knife{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", handy='" + handy + '\'' +
        ", origin='" + origin + '\'' +
        ", visualChar=" + visualChar +
        ", isSerial=" + isSerial +
        '}';
  }

  public String getInfo(){
    return "Knife{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", handy='" + handy + '\'' +
        ", origin='" + origin + '\'' +
        ", blade length=" + visualChar.getBlade().getLength() +
        ", blade width=" + visualChar.getBlade().getWidth() +
        ", blade material=" + visualChar.getBlade().getMaterial() +
        ", handle material=" + visualChar.getHandle().getMaterial() +
        ", handle details=" + visualChar.getHandle().getDetail() +
        ", bloodstream=" + visualChar.getBlade().isBloodstream() +
        ", isSerial=" + isSerial +
        '}';
  }

  public Knife() {
  }

  public Visual getVisualChar() {
    return visualChar;
  }

  public void setVisualChar(Visual visualChar) {
    this.visualChar = visualChar;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getHandy() {
    return handy;
  }

  public void setHandy(String handy) {
    this.handy = handy;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public boolean isSerial() {
    return isSerial;
  }

  public void setSerial(boolean serial) {
    isSerial = serial;
  }
}
