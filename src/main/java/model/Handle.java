package model;

public class Handle {

  private String material;
  private String detail;

  Handle(String material, String detail) {
    this.detail = detail;
    this.material = material;
  }

  public Handle() {
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }
}
